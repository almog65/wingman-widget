import React, {useState} from 'react';
import styled, {keyframes} from 'styled-components';
import { bounce , swing} from 'react-animations';
import 'font-awesome/css/font-awesome.css'
import Popup from './Popup'
import Avatars from './Avatars';
import './style.scss'
import {openNewSession, joinSession} from '../../utils/actions'
import {
    STATUS_INIT,
    STATUS_START_SESSION,
    STATUS_JOINED_SESSION,
} from '../../utils/constants'

const bounceAnimation = keyframes`${bounce}`;
const bounceInAnimation = keyframes`${swing}`;

const Root = styled.div`
    animation: 1s ${bounceAnimation};
    z-index: 10000000000000;
`

const Widget = () => {

    const [showPopup, setShowPopup] = useState(false)
    const [status, setStatus] = useState(STATUS_INIT)
    const [code, setCode] = useState(null)
    const [menuOpen, setMenuOpen] = useState(false)
    const {bidalgoWingman} = window

    const openNewSession = () => {
        if(status !== STATUS_START_SESSION){
            setStatus(STATUS_START_SESSION)
            setShowPopup(true);
            console.log('new');
            let code = '';
            if(bidalgoWingman) {
               code = bidalgoWingman.newSession();
            }
            console.log("code from server:", code);
            setCode(code)
        }else{
            console.log('left');
            setStatus(STATUS_INIT)
            if(bidalgoWingman) {
                bidalgoWingman.stopSession();
            }
        }
    }

    const joinSession = () => {
        if(status !== STATUS_JOINED_SESSION){
            setStatus(STATUS_JOINED_SESSION)
            setShowPopup(true);
            console.log('join');
        }else{
            console.log('left');
            setStatus(STATUS_INIT)
            if(bidalgoWingman) {
                bidalgoWingman.leaveSession();
            }
        }
    }

    const onJoinWithCode = (code) => {
        console.log(code);
        setShowPopup(false);
        if(bidalgoWingman) {
            bidalgoWingman.joinSession(code);
        }
    }

    return (
       <Root>
           <nav className="menu">
               <input type="checkbox" href="#" className="menu-open" name="menu-open" id="menu-open"/>
               <label className="menu-open-button" for="menu-open" onClick={() => setMenuOpen(!menuOpen)}>
                   <span className="hamburger hamburger-1"></span>
                   <span className="hamburger hamburger-2"></span>
                   <span className="hamburger hamburger-3"></span>
               </label>
               <a className="menu-item" onClick={() => openNewSession()}>
                   {status !== STATUS_START_SESSION && <i title="invite" className="fa fa-phone"></i>}
                   {status === STATUS_START_SESSION && <i title="leave session" className="fa fa-stop-circle"></i>}
               </a>
               <a className="menu-item" onClick={() => joinSession()}>
                   {status !== STATUS_JOINED_SESSION && <i title="join" className="fa fa-plus"></i>}
                   {status === STATUS_JOINED_SESSION && <i title="leave session" className="fa fa-minus"></i>}
               </a>
               <a className="menu-item">
                   {menuOpen && <Avatars />}
               </a>
               {(showPopup && menuOpen && status !== STATUS_INIT &&
                   (<Popup code={code} status={status} setShowPopup={setShowPopup} onJoinWithCode={onJoinWithCode}/>))}
           </nav>
           <svg xmlns="http://www.w3.org/2000/svg" version="1.1">
               <defs>
                   <filter id="shadowed-goo">

                       <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="10" />
                       <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo" />
                       <feGaussianBlur in="goo" stdDeviation="3" result="shadow" />
                       <feColorMatrix in="shadow" mode="matrix" values="0 0 0 0 0  0 0 0 0 0  0 0 0 0 0  0 0 0 1 -0.2" result="shadow" />
                       <feOffset in="shadow" dx="1" dy="1" result="shadow" />
                       <feComposite in2="shadow" in="goo" result="goo" />
                       <feComposite in2="goo" in="SourceGraphic" result="mix" />
                   </filter>
                   <filter id="goo">
                       <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="10" />
                       <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo" />
                       <feComposite in2="goo" in="SourceGraphic" result="mix" />
                   </filter>
               </defs>
           </svg>
       </Root>
    )
}

export default Widget