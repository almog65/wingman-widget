import React, {useState, useEffect} from 'react';
import styled, {keyframes} from 'styled-components';
import getAvatars from '../../utils/loadAvatars'

const Root = styled.div`
    display: flex;
    flex-direction: column;
    img:hover i {
       text-decoration: none !important;
    }
    z-index: 10000000000000;
`

const Img = styled.img`
    width: 69px;
    height: 72px;
    top: -19px;
    position: relative;
    left: -5px;
    &:hover {
        filter: invert(1);
    }
`

const ArrowUp = styled.i`
    font-size: 13px;
    color: #00B8FF !important;
    position: relative;
    top: -18px;
    z-index: 10000000000000;
`

const ArrowDown = styled.i`
    font-size: 13px;
    color: #00B8FF !important;
    position: relative;
    top: -20px;
    z-index: 10000000000000;
`

const Avatars = () => {

    useEffect(() => {
        setAvatarsImg(getAvatars())
    }, [])

    const [currentImg, setCurrentImg] = useState(1);
    const [avatarsImg, setAvatarsImg] = useState([]);

    const switchAvatar = (dir) => {
        const maxIndex = Object.keys(avatarsImg).length;
        if (dir == 'up'){
            if(currentImg + 1 > maxIndex){
                setCurrentImg(1)
            }else{
                setCurrentImg(currentImg + 1)
            }
        } else {
           if(currentImg === 1){
               setCurrentImg(maxIndex)
           }else{
               setCurrentImg(currentImg - 1)
           }
        }
        console.log(currentImg);
    }

    return (
        <Root>
            <ArrowUp className="fa fa-chevron-up" onClick={() => switchAvatar('up')} />
            <Img id="wingmanAvatar" src={avatarsImg[`i${currentImg}`]} onClick={() => switchAvatar('up')} />
            <ArrowDown className="fa fa-chevron-down" onClick={() => switchAvatar('down')} />
        </Root>
    )
}

export default Avatars