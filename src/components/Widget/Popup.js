import React, {useState} from 'react';
import styled, {css} from 'styled-components'
import {
    STATUS_INIT,
    STATUS_START_SESSION,
    STATUS_JOINED_SESSION,
} from '../../utils/constants'

const Root = styled.a`
  background-color: rgba(255, 255, 255, 0.6) !important;
  width: auto !important;
  padding: 0px 20px !important;
  color: #057aff !important;
  text-decoration: none !important;
  display: flex !important;
  border-radius: 10px !important;
  margin-left: -32px !important;
  align-items: center;
  justify-content: center;
  .fa-times {
    align-items: center;
    justify-content: center;
    line-height: 3;
    top: 2px;
    position: relative;
    margin-left: 20px;
  }
`

const Text = styled.div`
    font-size: 14px;
`

const JoinWrapper = styled.div`
  display: flex;
  padding: 0;
  
`

const InputText = styled.input`
  -webkit-appearance: none;
  box-sizing: border-box;
  padding-left: 10px;
  border-radius: 40px;
  font-size: 14px;
  border: 1px solid #00B8FF;
  color: #00B8FF;
  background: none;
  height: 40px;
  width: 220px;
  margin: 0;
  transition: 0.3s;
  box-shadow: inset 0 0 0 none;
  &:hover {
    color: #fff;
    box-sizing: border-box;
    border: 3px solid #00B8FF;
    background: #00B8FF;
  }
  &:focus{
    background: #00B8FF;
    border: 3px solid #00B8FF;
    color: #fff;
    outline: none;
  }
`

const Btn = styled.input`
  -webkit-appearance: none;
  text-transform: uppercase;
  border-radius: 30px;
  height: 40px;
  width: 70px;
  color: white !important;
  background: #00B8FF;
  border: none;
  margin-left: 10px;
  cursor: pointer;
  transition-duration: 0.3s;
  font-size: 14px;
    align-content: center;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
  &:hover {
    background: none;
    color: #00B8FF !important;
    border: 3px solid #00B8FF;
  }
  &:active {
    border: none;
    outline: none;
  }
  &:visited {
    border: none;
    outline: none;
  }
  ${({disabled}) => disabled && css`
    pointer-events: none;
    opacity: 0.3;
  `}
`

const Popup = ({setShowPopup, status, code, onJoinWithCode}) => {
    const [inputCode, setInputCode] = useState(null);
    return (
        <Root className="menu-item">
            {status === STATUS_START_SESSION && (
                <Text>Your session code: {code}</Text>
            )}
            {status === STATUS_JOINED_SESSION && (
                <JoinWrapper class="input-container">
                    <InputText type="text" id="txtSearch" placeholder="Insert Code" value={inputCode} onChange={(e) => setInputCode(e.target.value)} />
                    <Btn type="button" id="btnSearch" value="Send" disabled={!inputCode} onClick={() => onJoinWithCode(inputCode)} />
                </JoinWrapper>
            )}
            <i className="fa fa-times" onClick={() => setShowPopup(false)}></i>
        </Root>
    )
}

export default Popup