import i1 from '../images/1.png';
import i2 from '../images/2.png';
import i3 from '../images/3.png';
import i4 from '../images/4.png';
import i5 from '../images/5.png';
import i6 from '../images/6.png';
import i7 from '../images/7.png';
import i8 from '../images/8.png';
import i9 from '../images/9.png';
import i10 from '../images/10.png';
import i11 from '../images/11.png';
import i12 from '../images/12.png';
import i13 from '../images/13.png';
import i14 from '../images/14.png';
import i15 from '../images/15.png';
import i16 from '../images/16.png';
import i17 from '../images/17.png';
import i18 from '../images/18.png';


const getAvatars = () => {
    return {
        i1, i2, i3, i4, i5, i6, i7, i8, i9, i10,
        i11, i12, i13, i14, i15, i16, i17, i18
    }
}

export default getAvatars