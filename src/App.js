import React from 'react';
import './App.css';
import Widget from './components/Widget/Widget'
import styled from 'styled-components';

const Root = styled.div`
  position: absolute;
  bottom: -30px;
  left: 10px;
  z-index: 1000000;
  font-family: 'Open Sans',sans-serif;
}
`

function App() {
  return (
    <Root className="App">
      <Widget />
    </Root>
  );
}

export default App;
